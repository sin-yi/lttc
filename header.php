<!-- 電腦版header -->
<header class="patheaderBk js-patheaderBk">
<a href="index.php" class="patheaderBk-logo" title="">
		<img src="images/logo.svg" alt="語言訓練測驗中心LOGO" class="">
	</a>
	<div class="patheaderBk-linkBk">
		<a href="exhibits.php" class="patheaderBk-linkBk--link mr-50">
			<span class="ch">線上特展</span>
			<span class="en">e-Exhibits</span>
		</a>
		
		<div class="patheaderBk-linkBk--link mr-50">
			<div class="dropdown">
				<a href="javascript:void(0);" class="dropdown-tit">
					<span class="ch">重點活動</span>
					<span class="en">Events</span>
				</a>
				<!-- The content -->
				<div class="dropdown__content plr-20 pt-40">
					<a href="opening.php" class="dropdown__content_link">
						<span class="ch-content pb-20">慶祝茶會</span>
						<span class="en-content pb-20">Opening Ceremony</span>
					</a>
					<a href="opening.php#js-pagOpSection02-ahref" class="dropdown__content_link pb-20">
						<span class="ch-content pb-20">現場展覽</span>
						<span class="en-content pb-20">Exhibition Gallery</span>
					</a>
				</div>
			</div>
		</div>

		<a href="wish.php" class="patheaderBk-linkBk--link mr-50">
			<span class="ch">各方祝福</span>
			<span class="en">Well-wishers</span>
		</a>

		<a href="https://www.lttc.ntu.edu.tw/en-trends" target="_blank" class="patheaderBk-linkBk--link mr-50">
			<span class="ch">學外語送好禮</span>
			<span class="en">Prize Draw</span>
		</a>

		<a href="https://docs.google.com/forms/d/e/1FAIpQLSd3YzTYE2pRYrWETH8OTq7kxUbQ4Weira-_OoKvnZLv-qjnHA/viewform" class="patheaderBk-linkBk--link mr-50" target="_blank">
			<span class="ch">有獎徵答</span>
			<span class="en">Prize Quiz</span>
		</a>

		<a href="https://70anniversary.lttc.tw/en/" class="patheaderBk-linkBk--link">
			<span class="ch">English</span>
			<span class="en">English</span>
		</a>
		
		
	</div>
</header>
