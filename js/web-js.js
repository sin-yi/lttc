/*網站區js*/
$(document).ready(function() {
	
	/* 滑動消失header的logo*/
	// $(window).scroll(function() {
	// 	// alert(123);
	// 	if($(window).scrollTop() >= 800){
	// 		// $(".patSmlHeader").show();
	// 		TweenMax.to(".js-headerLogo", 0.2, {ease: Power4.easeInOut, y: -200 });
	// 	}else if($(window).scrollTop()<= 800){
	// 		// $(".patSmlHeader").hide();
	// 		TweenMax.to(".js-headerLogo", 0.2, {ease: Power4.easeInOut, y: 0 });
	// 	}
	// });



	//小視口通知按鈕
	// $(".jsNoteBt").on('click',function(){
	// 	// console.log("我有觸發click事件");
	// 	$(".jsNavSmall").animate({
	// 	  left: '-320',
	// 	});
	// 	$(".jspatSmlNavNote").animate({
	// 		left: '0',
	// 	});
	// 	$(".jsNavSml-openbg").show();
	// });
	// $(".jsNavSmlNote-bt--close").on('click',function(){
	// 	// console.log("我有觸發關閉事件");
	// 	$(".jspatSmlNavNote").animate({
	// 		left: '-320',
	// 	});
	// 	$(".jsNavSml-openbg").fadeOut(500);
	// });
	

	/*classify 打開select*/
	// $(".js-eleClassifyBt").on('click',function(){
	// 	if($(".js-eleClassifySel").css("display") == "none" ){
    //         $(".js-eleClassifySel").slideDown(300);
	// 	}else if($(".js-eleClassifySel").css("display") == "block" ){
	// 		$(".js-eleClassifySel").slideUp(300);
	// 	}
	// });



	// 出現錨點
	// $(".jsindthumbArea").css("display","none");
	// $(window).scroll(function(){
	// 	var bannerHeight = $(".indBanner").height();
	// 	// console.log("banner高度:"+bannerHeight);
	// 	var showBtH = bannerHeight - 150;
	// 	// console.log(bannerHeight + "- 150 = "+showBtH);
	// 	// 該元素只有適口1856px出現,如果抓不到,判斷式不會觸發
	// 	var eleHeight = $(".indele-pro05").offset().top;
	// 	var winHeight = $(window).height();
	// 	// console.log(eleHeight);
	// 	// console.log(winHeight);
	// 	var hidBtH = eleHeight - winHeight + 100;
	// 	// console.log("卷軸關閉高度"+hidBtH);
	// 	// console.log($(window).scrollTop());
	// 	if($(window).scrollTop()>= showBtH && $(window).scrollTop()<= hidBtH){
	// 		$(".jsindthumbArea").css("display","block");
	// 		TweenMax.staggerTo(".jsthumb", 0, {transform: "translateY(0px)", opacity:1, delay:0}, 0.1);
	// 	}else{
	// 		// console.log("關閉");
	// 		TweenMax.to(".jsthumb", 0.5, {transform: "translateY(-50px)", opacity:0, delay:0}, 0);
	// 	}
	// });



});

// window.onload會等網頁的全部內容，包括圖片，CSS及<iframe>等外部內容載入後才會觸發，但$(document).ready()在Document Object Model (DOM) 載入後就會觸發，所以順序上$(document).ready()會比window.onload先執行。
//window.onload是JavaScript的原生事件，而$(document).ready()是jQuery的事件（其實是透過監聽JavaScript的DOMContentLoaded事件來實現）。
//loading特效
$(window).ready(function(){
	//loading動畫
	var tl04 = gsap.timeline();
	gsap.to(".js-dot01", {
		// opacity: 1,
		x: 30,
		duration: 1.5,
		repeat: -1,
		scale: 1,
		// top: 0,
		ease: {ease: Power3.easeInOut, y: 0 },
	});
	gsap.to(".js-dot02", {
		opacity: 1,
		x: 30,
		duration: 1.5,
		repeat: -1,
		scale: 0,
		delay: 1.5,
		// top: 0,
		ease: {ease: Power3.easeInOut, y: 0 },
	});        
});
$(window).on('load',function(){
	//loading結束
	gsap.to(".js-loadingEnd", {
		opacity: 0,
		duration: 1,
		zIndex: -10,
		delay: 1.5,
		// top: 0,
		ease: {ease: Power3.easeInOut, y: 0 },
	});
});


$(document).ready(function() {

	// 大視口導覽列
	/*往上捲出現導覽列,往下隱藏*/
	let windowY = 0 ;
	const window500 = 500 ;
	$(window).scroll(function(){
		let nowY = window.pageYOffset ;
		// console.log("現在y:" + nowY);
		if (nowY > window500){
			// console.log("變換底色");
			$(".js-patheaderBk").addClass("patheaderBk--scrollStyle");
		}else {
			// console.log("去除底色");
			$(".js-patheaderBk").removeClass("patheaderBk--scrollStyle");
		}
	});
	

	/*小視口導覽列開合*/
	$(".jsNavSml-bt").on('click',function(){
		// console.log("123");
		// $(".patSmlHeader-navbt--icon01").addClass("patSmlHeader-navbt--icon01--rotate");
		// $(".patSmlHeader-navbt--icon02").addClass("patSmlHeader-navbt--icon02--rotate");
		// $(".jsNavSmall").slideToggle("slow");
		if($(".jsNavSmall").css("display") == "none"){
			$(".jsNavSmall").slideDown("slow");
			$(".patSmlHeader-navbt--icon01").addClass("patSmlHeader-navbt--icon01--rotate");
			$(".patSmlHeader-navbt--icon02").addClass("patSmlHeader-navbt--icon02--rotate");
		}else{
			$(".jsNavSmall").slideUp("slow");
			$(".patSmlHeader-navbt--icon01").removeClass("patSmlHeader-navbt--icon01--rotate");
			$(".patSmlHeader-navbt--icon02").removeClass("patSmlHeader-navbt--icon02--rotate");
		}
	});


	/*二層開合特效*/
	$(".jsAccording").stop(true).click(function(){
		if($(this).children(".jsAccording-secondArea").css("display") == "none" ){
			$(".jsAccording > .jsAccording-secondArea").slideUp(200);
            $(this).children(".jsAccording-secondArea").slideDown(200);
		}else if($(this).children(".jsAccording-secondArea").css("display") == "block"){
			$(this).children(".jsAccording-secondArea").slideUp(200);
		};
	});
	/*這是測試bit bucket的檔案能否推送*/
	$(".jsAccording > .jsAccording-secondArea > a").on('click',function(){
		$(".jsNavSmall").slideUp("slow");
		$(".patSmlHeader-navbt--icon01").removeClass("patSmlHeader-navbt--icon01--rotate");
		$(".patSmlHeader-navbt--icon02").removeClass("patSmlHeader-navbt--icon02--rotate");
	});

	/*語言區開合特效*/
	$(".jsLanAccording").stop(true).click(function(){
		if($(this).children(".jsLanAccording-sel").css("display") == "none" ){
			// console.log("123");
			$(this).children(".jsLanAccording-sel").slideDown(200);
			$(".jsLanAccording-arrow").addClass("patBigNav-selLan--arrow--rotate");
		}else if($(this).children(".jsLanAccording-sel").css("display") == "block"){
			$(this).children(".jsLanAccording-sel").slideUp(200);
			$(".jsLanAccording-arrow").removeClass("patBigNav-selLan--arrow--rotate");
		};
	});
	
	//回頁頂特效
	$(".modTopBtBk").click(function(){
		$("html,body").animate({
			scrollTop:0,
		},300);
	});
	
	
	

});